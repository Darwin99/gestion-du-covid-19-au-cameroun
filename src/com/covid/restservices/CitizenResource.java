package com.covid.restservices;


import java.net.*;
import java.util.*;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import com.covid.beans.Agent;
import com.covid.beans.Citoyen;
import com.covid.beans.Menage;
import com.covid.services.AgentManagement;
import com.covid.services.CitoyenManagement;

@Path("/citizens")
public class CitizenResource {
	CitoyenManagement citoyenManagement = new CitoyenManagement();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response list() {
		ArrayList<Citoyen> citoyens =  citoyenManagement.getAll();
		 return Response
		            .status(Response.Status.OK)
		            .header("Access-Control-Allow-Origin", "*")
		            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
		            .entity(citoyens)
		            .build();
	}
	
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") int id) {
	    Citoyen citoyen = citoyenManagement.getOne(id);
	    
	    if (citoyen != null) {
	        return Response.ok(citoyen, MediaType.APPLICATION_JSON).header("Access-Control-Allow-Origin", "*").build();
	    } else {
	        return Response.status(Response.Status.NOT_FOUND).build();
	    }
	}
	
	@POST
	@Path("{id_menage}/{id_agent}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(Citoyen ct,@PathParam("id_menage") int id_menage,@PathParam("id_agent") int id_agent){
		AgentManagement agentManager = new AgentManagement();		
		Agent agent = agentManager.getOne(id_agent);
		Menage menage = new Menage(9);
		ct.setAgent(agent);
		ct.setMenage(menage);
		
	    citoyenManagement.create(ct);
	    return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@POST
	@Path("{id_menage}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add2(Citoyen ct,@PathParam("id_menage") int id_menage){
		Menage menage = new Menage(9);
		ct.setMenage(menage);
		
	    citoyenManagement.create(ct);
	    return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int id) {
		CitoyenManagement ctManager = new CitoyenManagement();
		int rs = ctManager.delete(id);
	    if (rs == 1) {
	        return Response.ok().header("Access-Control-Allow-Origin", "*").build();
	    } else {
	        return Response.notModified().header("Access-Control-Allow-Origin", "*").build();
	    }
	}

	
	@PUT
	@Path("{id}/{id_menage}/{id_agent}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(Citoyen ct,@PathParam("id") int id,@PathParam("id_menage") int id_menage,@PathParam("id_agent") int id_agent){
		AgentManagement agentManager = new AgentManagement();		
		Agent agent = agentManager.getOne(id_agent);
		Menage menage = new Menage(9);
		ct.setAgent(agent);
		ct.setMenage(menage);
		
	    citoyenManagement.update(id,ct);
	    
	    return Response.status(Response.Status.OK).header("Access-Control-Allow-Origin", "*").build();
	}
	
}
