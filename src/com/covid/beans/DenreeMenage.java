package com.covid.beans;

import java.util.*;

public class DenreeMenage {
   /** @pdOid 6e7c0be0-70c2-443d-86bc-0a7665748113 */
   private int quantite;
   
   private Denree denreeB;
   
   private Menage menage;
   private Agent agent;
   private DenreeMenage() {}

public DenreeMenage(int quantite, Denree denreeB, Menage menage) {
	super();
	this.quantite = quantite;
	this.denreeB = denreeB;
	this.menage = menage;
}



public Agent getAgent() {
	return agent;
}

public void setAgent(Agent agent) {
	this.agent = agent;
}

public int getQuantite() {
	return quantite;
}

public void setQuantite(int quantite) {
	this.quantite = quantite;
}

public Denree getDenreeB() {
	return denreeB;
}

public void setDenreeB(Denree denreeB) {
	this.denreeB = denreeB;
}

public Menage getMenage() {
	return menage;
}

public void setMenage(Menage menage) {
	this.menage = menage;
}
   

}