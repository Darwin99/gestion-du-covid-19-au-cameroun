package com.covid.beans;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class Region {
   /**
    * le construceur sans argument
    */
   public Region() {}
	
	public Region(int id,String nom) {
		super();
		this.id = id;
		this.nom = nom;
	}
	
	
	/**
	 * Les attributs
	 */
	private int id;
	private String nom;
//	public java.util.Collection departements;
	
	// les getteurs et setteurs de l'application
	public int getId() { return id; }
	
	public void setId(int id) { this.id = id; }
	
	public String getNom() { return nom; }
	
	public void setNom(String nom) { this.nom = nom; }
	
	}