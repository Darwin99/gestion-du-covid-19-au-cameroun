package com.covid.beans;

import java.util.*;


public class DenreeRegion {
   /** @pdOid cc480d92-dd04-43bc-a5e8-125b4b38767f */
   private long quantite;
   
   private Denree denreeB;
   
   private Region region;
   
   public DenreeRegion() {}

public DenreeRegion(int quantite, Denree denreeB, Region region) {
	super();
	this.quantite = quantite;
	this.denreeB = denreeB;
	this.region = region;
}

public long getQuantite() {
	return quantite;
}

public void setQuantite(int quantite) {
	this.quantite = quantite;
}

public Denree getDenreeB() {
	return denreeB;
}

public void setDenreeB(Denree denreeB) {
	this.denreeB = denreeB;
}

public Region getRegion() {
	return region;
}

public void setRegion(Region region) {
	this.region = region;
}
   
   

}