package com.covid.beans;

import java.util.*;

public class DenreeDepartement {
   /** @pdOid 90b3f5b7-273a-4f7e-a42c-68c4a6aad524 */
   private int quantite;
   
   public Denree denreeB;
   public Departement departement;
   
   public DenreeDepartement() {}

public DenreeDepartement(int quantite, Denree denreeB, Departement departement) {
	super();
	this.quantite = quantite;
	this.denreeB = denreeB;
	this.departement = departement;
}

public int getQuantite() {
	return quantite;
}

public void setQuantite(int quantite) {
	this.quantite = quantite;
}

public Denree getDenreeB() {
	return denreeB;
}

public void setDenreeB(Denree denreeB) {
	this.denreeB = denreeB;
}

public Departement getDepartement() {
	return departement;
}

public void setDepartement(Departement departement) {
	this.departement = departement;
}
   
   

}