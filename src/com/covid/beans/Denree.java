package com.covid.beans;

import java.util.*;

/** @pdOid 1fb5bedc-aa07-4d48-89b7-bdcaa97a3c68 */
public class Denree {
   /** @pdOid c7320c1f-93aa-4aa8-beb4-df7d8256de11 */
   private long id;
   /** @pdOid 3a72afc3-9aeb-4566-a0e2-eccf3df513bf */
   private String libelle;
   
   public Denree() {}

public long getId() {
	return id;
}

public Denree(int id, String libelle) {
	super();
	this.id = id;
	this.libelle = libelle;
}

public void setId(int id) {
	this.id = id;
}

public String getLibelle() {
	return libelle;
}

public void setLibelle(String libelle) {
	this.libelle = libelle;
}
   
}