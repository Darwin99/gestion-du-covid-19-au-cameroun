package com.covid.servles;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.covid.beans.Agent;
import com.covid.beans.Region;
import com.covid.services.AgentManagement;
import com.covid.services.RegionManagement;

/**
 * Servlet implementation class MaestrosTest
 */
@WebServlet("/MaestrosTest")
public class MaestrosTest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MaestrosTest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RegionManagement agentM = new RegionManagement();
		agentM.getConnexionInstance();
		
//		selection totale
		request.setAttribute("list", agentM.getAll().toArray());
		
//		selection individuele
		request.setAttribute("one_data", agentM.getOne(2));
		
		// creation et insertion
//		request.setAttribute("result_insert", "Resultat insertion = "+agentM.create(new Region(1,"Gaoudere")));
		
		// test de mise a jour
		request.setAttribute("result_update", "Resultat update = "+agentM.update(1,new Region(17,"new_region_update")));
		
		// test de la suppresion dans la table region
		request.setAttribute("result_delete", "Resultat delate = "+agentM.delete(8));

		
		
		this.getServletContext().getRequestDispatcher("/Maestros-tests/test.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
