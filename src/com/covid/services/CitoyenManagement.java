package com.covid.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.covid.beans.Agent;
import com.covid.beans.Citoyen;
import com.covid.beans.Menage;
import com.covid.beans.Secteur;

public class CitoyenManagement extends MariaDBConnexion {

	public CitoyenManagement() {
		super();
	}

	/**
	 * cette fonction renvoie toute les information conceran une seule entree de la
	 * base de donnees.
	 * 
	 * 
	 * @param id
	 * @return
	 */
	public Citoyen getOne(long id) {

		ResultSet result = null;
		Citoyen citoyen = new Citoyen();
		try {

			PreparedStatement statement = connexion.prepareStatement("SELECT * FROM citoyen WHERE id=?",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			statement.setLong(1, id);
			result = statement.executeQuery();
			if (result.first()) {
				// creation de l'objet

				citoyen.setId(result.getInt("id"));
				citoyen.setNom(result.getString("nom"));
				citoyen.setLogin(result.getString("prenom"));
				citoyen.setDateNaissance(result.getString("date_naissance"));
				citoyen.setSexe(result.getString("sexe"));
				citoyen.setImage(result.getString("image"));
				citoyen.setPassword(result.getString("password"));
				citoyen.setProfession(result.getString("profession"));
				citoyen.setEstContamine(result.getBoolean("est_contamine"));
				Agent agent = new Agent();
				agent.setId(result.getInt("id_agent"));
				citoyen.setAgent(agent);

				Menage menage = new Menage();
				menage.setId(result.getLong("id_menage"));

				citoyen.setMenage(menage);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();

		}
		return citoyen;

	}

	public ArrayList<Citoyen> getAll() {
		ResultSet result = null;
		ArrayList<Citoyen> list_datas = new ArrayList<Citoyen>();

		try {

			PreparedStatement statement = connexion.prepareStatement("SELECT * FROM citoyen",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			result = statement.executeQuery();
			while (result.next()) {
				// creation de l'objet
				Citoyen citoyen = new Citoyen();
				citoyen.setId(result.getInt("id"));
				citoyen.setNom(result.getString("nom"));
				citoyen.setLogin(result.getString("prenom"));
				citoyen.setDateNaissance(result.getString("date_naissance"));
				citoyen.setSexe(result.getString("sexe"));
				citoyen.setImage(result.getString("image"));
				citoyen.setPassword(result.getString("password"));
				citoyen.setProfession(result.getString("profession"));
				citoyen.setEstContamine(result.getBoolean("est_contamine"));
				Menage menage = new Menage();
				menage.setId(result.getInt("id_menage"));
				citoyen.setMenage(menage);
				Agent agent = new Agent();
				agent.setId(result.getLong("id_agent"));
				list_datas.add(citoyen);
				
			}
		} catch (SQLException ex) {
			ex.printStackTrace();

		}
		
		System.out.println(list_datas);
		return list_datas;
	}

	/**
	 * 
	 * @param citoyen
	 * @return
	 */

	public int create(Citoyen citoyen) {
		int rset = 0;
		try {

			if (citoyen.getAgent() == null) {

				PreparedStatement statement = connexion.prepareStatement(
						"INSERT INTO citoyen(login, password, nom, prenom, date_naissance, sexe, image, profession, est_contamine, id_menage) VALUES(?,?,?,?,?,?,?,?,?,?)");

				statement.setString(1, citoyen.getLogin());
				statement.setString(2, citoyen.getPassword());
				statement.setString(3, citoyen.getNom());
				statement.setString(4, citoyen.getPrenom());
				statement.setString(5, citoyen.getDateNaissance());
				statement.setString(6, citoyen.getSexe());
				statement.setString(7, citoyen.getImage());
				statement.setString(8, citoyen.getProfession());
				statement.setBoolean(9, citoyen.isEstContamine());

				statement.setLong(10, citoyen.getMenage().getId());

				rset = statement.executeUpdate();
//					rset =state.executeUpdate(query);

			} else {
				PreparedStatement statement = connexion.prepareStatement(
						"INSERT INTO citoyen(login, password, nom, prenom, date_naissance, sexe, image, profession, est_contamine, id_agent, id_menage) VALUES(?,?,?,?,?,?,?,?,?,?,?)");

				statement.setString(1, citoyen.getLogin());
				statement.setString(2, citoyen.getPassword());
				statement.setString(3, citoyen.getNom());
				statement.setString(4, citoyen.getPrenom());
				statement.setString(5, citoyen.getDateNaissance());
				statement.setString(6, citoyen.getSexe());
				statement.setString(7, citoyen.getImage());
				statement.setString(8, citoyen.getProfession());
				statement.setBoolean(9, citoyen.isEstContamine());

				statement.setLong(10, citoyen.getAgent().getId());
				statement.setLong(11, citoyen.getMenage().getId());

				rset = statement.executeUpdate();
			}

//			rset =state.executeUpdate(query);
		} catch (SQLException sq) {
			sq.printStackTrace();

		}
		return rset;
	}

	/**
	 * 
	 * @param id
	 * @param citoyen
	 * @return
	 */

	public int update(int id, Citoyen citoyen) {

		int rset = 0;
		try {
			PreparedStatement statement = connexion.prepareStatement(
					"UPDATE citoyen SET login =?, password=?, nom=?, prenom=?, date_naissance=?, sexe=?, image=?, profession=?, est_contamine=?, id_menage=?, id_agent=? WHERE id =?");

			statement.setString(1, citoyen.getLogin());
			statement.setString(2, citoyen.getPassword());
			statement.setString(3, citoyen.getNom());
			statement.setString(4, citoyen.getPrenom());
			statement.setString(5, citoyen.getDateNaissance());
			statement.setString(6, citoyen.getSexe());
			statement.setString(7, citoyen.getImage());
			statement.setString(8, citoyen.getProfession());
			statement.setBoolean(9, citoyen.isEstContamine());
			statement.setLong(10, citoyen.getMenage().getId());
			statement.setLong(11, citoyen.getAgent().getId());
			statement.setLong(12, id);

			statement.executeUpdate();
//			rset =state.executeUpdate(query);
		} catch (SQLException sq) {
			sq.printStackTrace();

		}
		return rset;
	}

	public int delete(int id) {

		int result = 0;
		try {
			PreparedStatement statement = connexion.prepareStatement("DELETE FROM citoyen WHERE id =?");
			statement.setLong(1, id);
			result = statement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

}
