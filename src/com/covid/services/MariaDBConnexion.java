package com.covid.services;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MariaDBConnexion {

	// URL de connexion à la base de données
	private static final String url = "jdbc:mariadb://localhost:3306/covid_19_management";

	// Utilisateur qui souhaite se connecter
	private static final String user = "root";

	// Mot de passe de connexion
	private static final String password = "";

	
	// attributs partages aux classes filles
	protected String query;
	protected ResultSet rset;
	protected int resultat;
	protected static Connection connexion = null;
	
	public MariaDBConnexion() {
		connexion = this.getConnexionInstance();
	}

	public  Connection getConnexionInstance() {
		if (connexion == null) {
			try {
				
				//This is for mariaDB data base
				Class.forName("org.mariadb.jdbc.Driver"); 
				//This is for mysql driver
				/*
				 * Class.forName("com.mysql.jdbc.Driver"); 
				 */
				connexion = DriverManager.getConnection(url, user, password);
				
			} catch (SQLException e) {
				
				System.out.println("Failled!");
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				
				System.out.println("************** Failled for name *********************");
				e.printStackTrace();
			}
		}
		System.out.println("Connexion Succes!");
		return connexion;
	}

}
