package com.covid.services;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import com.covid.beans.Region;

/**
 * 
 * @author Maestros
 *
 */
public class RegionManagement extends MariaDBConnexion{

	/**
	 * Les fonction utilitaire de la classe :	 
	 * 	
	 * - getAll()	- Recuperer toutes les entrees d'une table
	 * - getOne()	- Recuperer tous les attributs d'une entree
	 * - delete()	- Sipprimer tous les attributs d'une entree
	 * - update()	- Mettre a jours les attributs d'une entre
	 * - create()   - Cree un bean correspondant
	 */

	
	
	/**
	 * cette fonction va aller dans la tabke indiquee en paramettre 
	 * et retourner une ArrayList contenant toutes les informatiion 
	 * concerant la table passee en paramettre.
	 * @param table_name
	 * @return
	 */
	
	public ArrayList<Region>  getAll(){
		query = "SELECT * FROM "+table_name+" WHERE 1;";
		list_datas = new ArrayList<Region>();
		
		try{
			PreparedStatement state=connexion.prepareStatement(query);
			rset = state.executeQuery(query);
			while(rset.next()){
				// creation de l'objet
				une_region = new Region();
				une_region.setId(rset.getInt("id"));
				une_region.setNom(rset.getString("nom"));
				list_datas.add(une_region);
			}
		}
		catch(SQLException ex){
			ex.printStackTrace();
			
		}
		return list_datas;
	}
	
	
	/**
	 * cette fonction renvoie toute les information conceran 
	 * une seule entree de la base de donnees.
	 * 
	 * 
	 * @param table_name
	 * @param id
	 * @return
	 */
	public Region getOne(int id){
		query = "SELECT * FROM "+table_name+" WHERE id = " + id; 

		try{
			PreparedStatement state=connexion.prepareStatement(query);
			rset = state.executeQuery(query);
			if(rset.next()){
				// creation de l'objet
				une_region = new Region();
				une_region.setId(rset.getInt("id"));    
				une_region.setNom(rset.getString("nom"));
			}
		}
		catch(SQLException ex){
			ex.printStackTrace();
			
		}
		return une_region;
	}
	
	
	/**
	 * Cett methode insert toute les donnes d'une ligne de la base de donnees
	 * 
	 * 
	 * @param datas
	 * @return
	 */
	public int create (Region region) {
		query ="INSERT INTO "+table_name+" (nom) VALUES ('"+region.getNom()+"')";
		int rset=0;
		try {
			PreparedStatement state =connexion.prepareStatement(query);
			rset =state.executeUpdate(query);
		}
		catch(SQLException sq){
			sq.printStackTrace();
		}
		return rset;
	}
	
	
	
	
	/**
	 * Cette fonction mes a jour les information d'une ligne de la base de donnees
	 * 
	 * 
	 * @param id
	 * @param region
	 * @return
	 */
	public int update (int id, Region region) {
		query = "UPDATE "+table_name+" SET nom='"+region.getNom()+"' WHERE id = "+id;
		int rset=0;
		try {
			PreparedStatement state =connexion.prepareStatement(query);
			rset =state.executeUpdate(query);
		}
		catch(SQLException sq){
			sq.printStackTrace();
		}
		return rset;
	}
	
	
	
	
	/**
	 * Cette fonction supprime tous les attributs d'une entree
	 * 
	 * @param id
	 * @return
	 */
	public int delete (int id) {
		query = "DELETE FROM "+table_name+" WHERE id = " + id;
		int rset=0;
		try {
			PreparedStatement state =connexion.prepareStatement(query);
			rset =state.executeUpdate(query);
		}
		catch(SQLException sq){
			sq.printStackTrace();
		}
		return rset;
	}


	
//		Les attributs
	ArrayList<Region> list_datas; // toutes les donnes recuperees dan sla tables
	Region une_region;
	String table_name = "region";

}
