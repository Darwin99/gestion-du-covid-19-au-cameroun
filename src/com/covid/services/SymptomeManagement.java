package com.covid.services;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.covid.beans.Citoyen;
import com.covid.beans.Symptome;

/**
 * 
 * @author Maestros
 *
 */
public class SymptomeManagement extends MariaDBConnexion {

	
	public SymptomeManagement() {
		super();
	}
	
	public int create(Symptome symptome, long idAgent ,long idCitoyen) {
		int rset = 0;
		try {

			

				PreparedStatement statement = connexion.prepareStatement("INSERT INTO symptome(libelle, id_agent,id_citoyen) VALUES(?,?,?)");

				statement.setString(1, symptome.getLibelle());
				statement.setLong(2, idAgent);
				statement.setLong(3, idCitoyen);


				rset = statement.executeUpdate();
//					rset =state.executeUpdate(query);

			

//			rset =state.executeUpdate(query);
		} catch (SQLException sq) {
			sq.printStackTrace();

		}
		return rset;
	}

	
	
	
}
