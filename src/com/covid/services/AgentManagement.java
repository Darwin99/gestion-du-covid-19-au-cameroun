package com.covid.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.covid.beans.Agent;
import com.covid.beans.Secteur;

/**
 * 
 * @author Maestros
 *
 */
public class AgentManagement extends MariaDBConnexion {

	/**
	 * Les fonction utilitaire de la classe :
	 * 
	 * - getAll() - Recuperer toutes les entrees d'une table - getOne() - Recuperer
	 * tous les attributs d'une entree - delete() - Sipprimer tous les attributs
	 * d'une entree - update() - Mettre a jours les attributs d'une entre - create()
	 * - Cree un bean correspondant
	 */

	public AgentManagement() {
		super();
	}

	/**
	 * cette fonction va aller dans la tabke indiquee en paramettre et retourner une
	 * ArrayList contenant toutes les informatiion concerant la table passee en
	 * paramettre.
	 * 
	 * @param table_name
	 * @return
	 */

	public ArrayList<Agent> getAll() {
		ResultSet result = null;
		list_datas = new ArrayList<Agent>();

		try {

			PreparedStatement statement = connexion.prepareStatement("SELECT * FROM agent",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			result = statement.executeQuery();
			while (result.next()) {
				// creation de l'objet
				Agent agent = new Agent();
				agent.setId(result.getInt("id"));
				agent.setNom(result.getString("nom"));
				agent.setLogin(result.getString("prenom"));
				agent.setDateNaissance(result.getString("date_naissance"));
				agent.setSexe(result.getString("sexe"));
				agent.setImage(result.getString("image"));
				agent.setPassword(result.getString("password"));
				agent.setProfession(result.getString("profession"));
				agent.setEstContamine(result.getBoolean("est_contamine"));
				Secteur secteur = new Secteur();
				secteur.setId(result.getInt("id_secteur"));
				agent.setSecteur(secteur);
				list_datas.add(agent);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();

		}
		return list_datas;
	}

	/**
	 * cette fonction renvoie toute les information conceran une seule entree de la
	 * base de donnees.
	 * 
	 * 
	 * @param id
	 * @return
	 */
	public Agent getOne(int id) {

		ResultSet result = null;
		Agent agent = new Agent();
		try {

			PreparedStatement statement = connexion.prepareStatement("SELECT * FROM agent WHERE id=?",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			statement.setLong(1, id);
			result = statement.executeQuery();
			if (result.first()) {
				// creation de l'objet

				agent.setId(result.getInt("id"));
				agent.setNom(result.getString("nom"));
				agent.setLogin(result.getString("prenom"));
				agent.setDateNaissance(result.getString("date_naissance"));
				agent.setSexe(result.getString("sexe"));
				agent.setImage(result.getString("image"));
				agent.setPassword(result.getString("password"));
				agent.setProfession(result.getString("profession"));
				agent.setEstContamine(result.getBoolean("est_contamine"));
				Secteur secteur = new Secteur();
				secteur.setId(result.getInt("id_secteur"));
				agent.setSecteur(secteur);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();

		}
		return agent;

	}

	/**
	 * Cett methode insert toute les donnes d'une ligne de la base de donnees
	 * 
	 * 
	 * @param datas
	 * @return
	 */
	public int create(Agent agent) {
		int rset = 0;
		try {
			PreparedStatement statement = connexion.prepareStatement(
					"INSERT INTO agent(login, password, nom, prenom, date_naissance, sexe, image, profession, est_contamine, id_secteur) VALUES(?,?,?,?,?,?,?,?,?,?)");

			statement.setString(1, agent.getLogin());
			statement.setString(2, agent.getPassword());
			statement.setString(3, agent.getNom());
			statement.setString(4, agent.getPrenom());
			statement.setString(5, agent.getDateNaissance());
			statement.setString(6, agent.getSexe());
			statement.setString(7, agent.getImage());
			statement.setString(8, agent.getProfession());
			statement.setBoolean(9, agent.isEstContamine());
			System.out.println(agent.getSecteur().getId());
			statement.setLong(10, agent.secteur.getId());

			statement.executeUpdate();
//			rset =state.executeUpdate(query);
		} catch (SQLException sq) {
			sq.printStackTrace();

		}
		return rset;
	}

	public int update(int id, Agent agent) {

		int rset = 0;
		try {
			PreparedStatement statement = connexion.prepareStatement(
					"UPDATE agent SET login =?, password=?, nom=?, prenom=?, date_naissance=?, sexe=?, image=?, profession=?, est_contamine=?, id_secteur=? WHERE id =?");

			statement.setString(1, agent.getLogin());
			statement.setString(2, agent.getPassword());
			statement.setString(3, agent.getNom());
			statement.setString(4, agent.getPrenom());
			statement.setString(5, agent.getDateNaissance());
			statement.setString(6, agent.getSexe());
			statement.setString(7, agent.getImage());
			statement.setString(8, agent.getProfession());
			statement.setBoolean(9, agent.isEstContamine());

			statement.setLong(10, agent.secteur.getId());
			statement.setLong(11, id);

			statement.executeUpdate();
//			rset =state.executeUpdate(query);
		} catch (SQLException sq) {
			sq.printStackTrace();

		}
		return rset;
	}

	/**
	 * Cette fonction mes a jour les information d'une ligne de la base de donnees
	 * 
	 * 
	 * @param id
	 * @param agent
	 * @return
	 */
	public int delete(int id) {

		int result = 0;
		try {
			PreparedStatement statement = connexion.prepareStatement("DELETE FROM agent WHERE id =?");
			statement.setLong(1, id);
			result = statement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	/*
	 * 
	 * `id` INTEGER NOT NULL AUTO_INCREMENT, `login` VARCHAR(255) UNIQUE NOT NULL,
	 * `password` VARCHAR(500) NOT NULL, `nom` VARCHAR(255) NOT NULL, `prenom`
	 * VARCHAR(255), `date_naissance` DATE NOT NULL, `sexe` VARCHAR(80) NOT NULL,
	 * `image` VARCHAR(255), `profession` VARCHAR(255), `est_contamine` BOOLEAN
	 * DEFAULT false, `id_secteur` INTEGER NOT NULL,
	 */

//		Les attributs
	ArrayList<Agent> list_datas; // toutes les donnes recuperees dan sla tables
	Agent un_agent;
	String table_name = "agent";

}
