package com.covid.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.covid.beans.Symptome;
import com.covid.services.SymptomeManagement;
import com.covid.servlets.citoyen.CitizenList;

/**
 * Servlet implementation class SymptomeServlet
 */
@WebServlet("/SymptomeServlet")
public class SymptomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SymptomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		SymptomeManagement symptomeManagement=new SymptomeManagement();
		long idCitoyen =Long.parseLong(request.getParameter("id_citoyen"));
	
		String fievre = request.getParameter("fievre");
		if (fievre!=null) {
			Symptome symptome=new Symptome();
			symptome.setLibelle(fievre);
			symptomeManagement.create(symptome, 1, idCitoyen);
		}
		
		this.getServletContext().getRequestDispatcher("/citizenList.html")
		.forward(request, response);
	}

}
