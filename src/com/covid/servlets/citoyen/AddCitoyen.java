package com.covid.servlets.citoyen;

import java.beans.Encoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.covid.beans.Citoyen;
import com.covid.beans.Menage;
import com.covid.services.CitoyenManagement;

/**
 * Servlet implementation class AddCitoyen
 */
@WebServlet("/AddCitoyen")
public class AddCitoyen extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static HashMap<String, String> errors;
	public static String result;
	public static String success = "";
	public static String failled = "";
	public static String chemin = "/WEB-INF/uploads";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddCitoyen() {
		super();
		errors = new HashMap<String, String>();
		result = "Echec de l'enregistrement";
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());

		this.getServletContext().getRequestDispatcher("/WEB-INF/forms/addCitizen.jsp").forward(request, response);

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// here, we have to recovery the given parameters
		Citoyen citoyen = new Citoyen();
		citoyen.setPassword(request.getParameter("password"));
		citoyen.setSexe(request.getParameter("sexe"));
		citoyen.setImage(request.getParameter("image"));
		citoyen.setDateNaissance(request.getParameter("dateDeNaissance"));
		citoyen.setEstContamine(false);
		citoyen.setAgent(null);
		Menage menage = new Menage();
		menage.setId(Integer.parseInt(request.getParameter("menage")));
		citoyen.setMenage(menage);

		try {
			this.validationNom(request.getParameter("nom"));

		} catch (Exception e) {
			// TODO: handle exception
			errors.put("nom", e.getMessage());
		}

		citoyen.setNom(request.getParameter("nom"));

		try {
			this.validationPrenom(request.getParameter("prenom"));

		} catch (Exception e) {
			// TODO: handle exception
			errors.put("prenom", e.getMessage());
		}

		citoyen.setPrenom(request.getParameter("login"));
		try {
			this.validationLogin(request.getParameter("login"));

		} catch (Exception e) {
			// TODO: handle exception
			errors.put("login", e.getMessage());
		}
		citoyen.setLogin(request.getParameter("login"));

		try {
			this.validationNom(request.getParameter("profession"));

		} catch (Exception e) {
			// TODO: handle exception
			errors.put("profession", e.getMessage());
		}

		citoyen.setProfession(request.getParameter("profession"));

		try {
			this.validationMotsDePasse(request.getParameter("password"), request.getParameter("confirmation"));

		} catch (Exception e) {
			// TODO: handle exception
			errors.put("password", e.getMessage());
		}

		if (errors.isEmpty()) {
			CitoyenManagement citoyenManagement = new CitoyenManagement();
			
			//Algorithm encryption
			
			
			int responseAdd = citoyenManagement.create(citoyen);
			System.out.println("success");
			success = "Ajout avec succes!";
		} else {
			failled = "Echec de l'ajout! Veillez reessayez.";
		}

		request.setAttribute("errors", errors);
		request.setAttribute("success", success);
		request.setAttribute("failled", failled);
		request.setAttribute("citoyen", citoyen);

		this.getServletContext().getRequestDispatcher("/WEB-INF/forms/addCitizen.jsp").forward(request, response);

	}

	//// Methode de verification

	/**
	 * Valide l'adresse mail saisie.
	 */
	private void validationEmail(String email) throws Exception {
		if (email != null && email.trim().length() != 0) {
			if (!email.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
				throw new Exception("Merci de saisir une adresse mail valide.");
			}
		} else {
			throw new Exception("Merci de saisir une adresse mail.");
		}
	}

	private void validationMotsDePasse(String motDePasse, String confirmation) throws Exception {

		if (motDePasse != null && motDePasse.trim().length() != 0 && confirmation != null
				&& confirmation.trim().length() != 0) {

			if (!motDePasse.equals(confirmation)) {
				throw new Exception("Les mots de passe entr�s sont diff�rents, merci de les saisir � nouveau.");

			} else if (motDePasse.trim().length() < 8) {

				throw new Exception("Les mots de passe doivent contenir au moins 8 caract�res.");

			}

		} else {

			throw new Exception("Merci de saisir et confirmer votre mot	de passe.");

		}

	}

	private void validationNom(String nom) throws Exception {
		if (nom != null && nom.trim().length() < 3) {
			throw new Exception("Le nom doit contenir au	moins 3 caract�res.");
		}
	}

	private void validationPrenom(String prenom) throws Exception {
		if (prenom != null && prenom.trim().length() < 3) {
			throw new Exception("Le prenom doit contenir au	moins 3  caract�res.");
		}
	}

	private void validationLogin(String login) throws Exception {
		if (login != null && login.trim().length() < 5) {
			throw new Exception("Le login doit contenir au	moins 5 caract�res.");
		}
	}

	private void upload(String fileName) throws Exception {
		
		fileName = fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1);
		InputStream contenuFichier = null;
		Part part = null;
		try {
			contenuFichier = part.getInputStream();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new Exception("Erreur de chargemet de l'image....");
		}
		
		
		try {
			ecrireFichier( contenuFichier, fileName, chemin );
			
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	
	private void ecrireFichier( InputStream contenu, String	nomFichier, String chemin ) throws Exception {
			/* Pr�pare les flux. */
			
		BufferedInputStream entree = null;
		
		BufferedOutputStream sortie = null;
		
		try {
		
			/* Ouvre les flux. */
			
			entree = new BufferedInputStream( contenu,10240); //taille tampon
			
			sortie = new BufferedOutputStream( new FileOutputStream(new File( chemin + nomFichier ) ),10240 );
			/*
			* Lit le fichier re�u et �crit son contenu dans un fichier sur le
			* disque.
			*/
			byte[] tampon = new byte[10240];
			int longueur = 0;
			while ( ( longueur = entree.read( tampon ) ) > 0 ) {
			
				sortie.write( tampon, 0, longueur );
			
			}
			} finally {
				
				sortie.close();
				entree.close();
					
			
			}
			
	}
			
}
