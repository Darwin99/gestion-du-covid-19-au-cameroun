package com.covid.servlets.citoyen;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.covid.beans.Citoyen;
import com.covid.services.CitoyenManagement;

/**
 * Servlet implementation class UpdateCitizen
 */
@WebServlet("/UpdateCitizen")
public class UpdateCitizen extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCitizen() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		long id = Integer.parseInt(request.getParameter("id"));
		CitoyenManagement citoyenManagement = new CitoyenManagement();
		
		Citoyen citoyen =citoyenManagement.getOne(id);
		request.setAttribute("citoyen", citoyen);
		this.getServletContext().getRequestDispatcher("/WEB-INF/forms/addCitizen.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//first i have to recovery the given id and search the correpondant citizen 
		
		
	}

}
