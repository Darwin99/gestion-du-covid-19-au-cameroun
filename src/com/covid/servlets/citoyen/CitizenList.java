package com.covid.servlets.citoyen;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.covid.beans.Citoyen;
import com.covid.services.CitoyenManagement;

/**
 * Servlet implementation class CitizenList
 */
@WebServlet("/CitizenList")
public class CitizenList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CitizenList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		CitoyenManagement citoyenManagement = new CitoyenManagement();
		ArrayList<Citoyen> citoyens = new ArrayList<Citoyen>();
		
		citoyens = citoyenManagement.getAll();
		request.setAttribute("citoyens", citoyens);
		this.getServletContext().getRequestDispatcher("/WEB-INF/citizen/citizenList.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
