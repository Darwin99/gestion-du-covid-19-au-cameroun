package com.covid.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.covid.bdd.Dashboard;
import com.covid.bdd.Statistique;

/**
 * Servlet implementation class StatistiqueServlet
 */
@WebServlet("/StatistiqueServlet")
public class StatistiqueServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StatistiqueServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		Dashboard symptomesMenage = new Dashboard();
		
		request.setAttribute("totalPers", symptomesMenage.totalPersonne());
		
		request.setAttribute("totalCont", symptomesMenage.totalContamine());

		request.setAttribute("totalMenage", symptomesMenage.totalMenage());
		

		request.setAttribute("totalDenree", symptomesMenage.totalMenage());
		
		
		
		
		
		
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/stats/dashboard.jsp").forward(request, response);
		
		
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
