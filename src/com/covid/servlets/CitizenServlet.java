package com.covid.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.covid.beans.Agent;
import com.covid.beans.Citoyen;
import com.covid.beans.Menage;
import com.covid.beans.Secteur;
import com.covid.services.CitoyenManagement;


/**
 * Servlet implementation class CitizenServlet
 */
@WebServlet("/CitizenServlet")
public class CitizenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CitizenServlet() {
        super();
        // TODO Auto-generated constructor stub
        
    }
    
	/*
	 * Connect_DB connect_DB = new
	 * Connect_DB("jdbc:mariaDB://localhost:3306/covid_19_management", "root","");
	 */
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/forms/addCitizen.jsp")
		.forward(request, response);
		
		Secteur secteur = new Secteur(1, "DOUALA V", null);
		Agent agent = new Agent();
		agent.setLogin("raoul");
		agent.setPassword("raoul");
		agent.setNom("raoul");
		agent.setDateNaissance("2021-06-08");
		agent.setSexe("Feminin");
		agent.setImage("images");
		agent.setProfession("Engineer");
		agent.setEstContamine(false);
		agent.setSecteur(secteur);
		agent.setId(1);
		
//		AgentManagement agentManagement = new AgentManagement();
//		agentManagement.create(agent);
//		agentManagement.update(1, agent);
//		agentManagement.delete(1);
		
//		System.out.println(agentManagement.getAll());
		
		CitoyenManagement citoyenManagement = new CitoyenManagement();
		
		Citoyen citoyen = new Citoyen();
		
		citoyen.setLogin("raoul");
		citoyen.setPassword("raoul");
		citoyen.setNom("DARWIN");
		citoyen.setDateNaissance("2021-06-08");
		citoyen.setSexe("Feminin");
		citoyen.setImage("images");
		citoyen.setProfession("Engineer");
		citoyen.setEstContamine(false);
		citoyen.setAgent(agent);
		citoyen.setPrenom("Raoul");
		
		Menage menage = new Menage();
		menage.setId(1);
		citoyen.setMenage(menage);
		citoyen.setId(1);
//		citoyenManagement.create(citoyen);
//		citoyen = citoyenManagement.getOne(1);
//		System.out.println(citoyen);

//		citoyenManagement.update(1, citoyen);
//		System.out.println(citoyen.getNom());
		
		citoyenManagement.delete(1);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
