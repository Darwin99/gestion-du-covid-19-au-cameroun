package com.covid.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.covid.bdd.Statistique;



public class DepartementsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public DepartementsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Statistique symptomesMenage = new Statistique(request);
		request.setAttribute("symptomesM", symptomesMenage.recupererSymptomeDept());
		this.getServletContext().getRequestDispatcher("/WEB-INF/stats/departement.jsp").forward(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
