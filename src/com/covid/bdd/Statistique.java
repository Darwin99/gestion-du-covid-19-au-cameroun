package com.covid.bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.covid.beans.*;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class Statistique {
	
	private String idGet = "rien";
	
	public Statistique(HttpServletRequest request) {
		this.idGet = request.getParameter("id");
	}
	
	public Statistique() {
		
	}
	
	
	public List<Resultat> recupererSymptomes(){
		
		
		
		List<Resultat> resultatsMenages = new ArrayList<Resultat>();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			
		}
		
		Connection connexion = null;
		Statement statement = null;
		ResultSet resultat = null;
		
		try {
			connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "root", "");
			

			
			statement = (Statement) connexion.createStatement();
			
			
			
			resultat = statement.executeQuery("SELECT R.nom, count(C.id) AS nbre, R.id FROM citoyen C, menage M, secteur S, departement D, region R WHERE C.est_contamine=1 AND C.id_menage=M.id AND M.id_secteur=S.id AND S.id_departement=D.id AND D.id_region=R.id GROUP BY R.nom");
				

			
			
			while(resultat.next())
			{
				String nom = resultat.getString("R.nom");
				int nombre = resultat.getInt("nbre");
				int id = resultat.getInt("R.id");
				
				Resultat res = new Resultat();
				res.setNom(nom);
				res.setNombre(nombre);
				res.setId(id);
				
				resultatsMenages.add(res);
			}
			
			
		}catch(SQLException e) {
			System.out.println("Erreur de connexion");
		}
		finally {
			try {
				if(resultat != null)
					resultat.close();
				if(statement != null)
					statement.close();
				if(connexion != null)
					connexion.close();
			}catch(SQLException ignore){}
		}
		
		return resultatsMenages;
	}
	

	public List<Resultat> recupererSymptomeDept(){
		
		System.out.println("valeur " +this.idGet);
		
		
		List<Resultat> resultatsMenages = new ArrayList<Resultat>();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			
		}
		
		Connection connexion = null;
		Statement statement = null;
		ResultSet resultat = null;
		
		try {
			connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "root", "");
			

			
			statement = (Statement) connexion.createStatement();
			
			PreparedStatement preparedstatement = (PreparedStatement) connexion.prepareStatement("SELECT D.nom, count(C.id) AS nbre, D.id FROM citoyen C, menage M, secteur S, departement D, region R WHERE C.est_contamine=1 AND C.id_menage=M.id AND M.id_secteur=S.id AND S.id_departement=D.id AND D.id_region=R.id AND R.id=? GROUP BY D.nom");
			
			System.out.println("Correct");
			
			int idGetInt = Integer.parseInt(this.idGet) ;
			
			preparedstatement.setInt(1, idGetInt);
			
			resultat = preparedstatement.executeQuery();
				

			
			
			
			while(resultat.next())
			{
				String nom = resultat.getString("D.nom");
				int nombre = resultat.getInt("nbre");
				int id = resultat.getInt("D.id");
				
				Resultat res = new Resultat();
				res.setNom(nom);
				res.setNombre(nombre);
				res.setId(id);
				
				resultatsMenages.add(res);
			}
			
			
		}catch(SQLException e) {
			System.out.println("Erreur de connexion");
		}
		finally {
			try {
				if(resultat != null)
					resultat.close();
				if(statement != null)
					statement.close();
				if(connexion != null)
					connexion.close();
			}catch(SQLException ignore){}
		}
		
		return resultatsMenages;
	
	
		
	}
	
	
	public List<Resultat> recupererSymptomeSect(){
		
		System.out.println("valeur " +this.idGet);
		
		
		List<Resultat> resultatsMenages = new ArrayList<Resultat>();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			
		}
		
		Connection connexion = null;
		Statement statement = null;
		ResultSet resultat = null;
		
		try {
			connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "root", "");
			

			
			statement = (Statement) connexion.createStatement();
			
			PreparedStatement preparedstatement = (PreparedStatement) connexion.prepareStatement("SELECT S.nom, count(C.id) AS nbre, S.id FROM citoyen C, menage M, secteur S, departement D, region R WHERE C.est_contamine=1 AND C.id_menage=M.id AND M.id_secteur=S.id AND S.id_departement=D.id AND D.id_region=R.id AND D.id=? GROUP BY S.nom");
			
			System.out.println("Correct");
			
			int idGetInt = Integer.parseInt(this.idGet) ;
			
			preparedstatement.setInt(1, idGetInt);
			
			
			resultat = preparedstatement.executeQuery();
				

			
			
			
			while(resultat.next())
			{
				String nom = resultat.getString("S.nom");
				int nombre = resultat.getInt("nbre");
				int id = resultat.getInt("S.id");
				
				Resultat res = new Resultat();
				res.setNom(nom);
				res.setNombre(nombre);
				res.setId(id);
				
				resultatsMenages.add(res);
			}
			
			
		}catch(SQLException e) {
			System.out.println("Erreur de connexion");
		}
		finally {
			try {
				if(resultat != null)
					resultat.close();
				if(statement != null)
					statement.close();
				if(connexion != null)
					connexion.close();
			}catch(SQLException ignore){}
		}
		
		return resultatsMenages;
	
	
		
	}
	
	
public List<Resultat> recupererSymptomeMenage(){
		
		System.out.println("valeur " +this.idGet);
		
		
		List<Resultat> resultatsMenages = new ArrayList<Resultat>();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			
		}
		
		Connection connexion = null;
		Statement statement = null;
		ResultSet resultat = null;
		
		try {
			connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "root", "");
			

			
			statement = (Statement) connexion.createStatement();
			
			PreparedStatement preparedstatement = (PreparedStatement) connexion.prepareStatement("SELECT M.nom, count(C.id) AS nbre, M.id FROM citoyen C, menage M, secteur S, departement D, region R WHERE C.est_contamine=1 AND C.id_menage=M.id AND M.id_secteur=S.id AND S.id_departement=D.id AND D.id_region=R.id AND S.id=? GROUP BY S.nom");
			
			System.out.println("Correct");
			
			int idGetInt = Integer.parseInt(this.idGet) ;
			
			preparedstatement.setInt(1, idGetInt);
			
			
			resultat = preparedstatement.executeQuery();
				

			
			
			
			while(resultat.next())
			{
				String nom = resultat.getString("M.nom");
				int nombre = resultat.getInt("nbre");
				int id = resultat.getInt("M.id");
				
				Resultat res = new Resultat();
				res.setNom(nom);
				res.setNombre(nombre);
				res.setId(id);
				
				resultatsMenages.add(res);
			}
			
			
		}catch(SQLException e) {
			System.out.println("Erreur de connexion");
		}
		finally {
			try {
				if(resultat != null)
					resultat.close();
				if(statement != null)
					statement.close();
				if(connexion != null)
					connexion.close();
			}catch(SQLException ignore){}
		}
		
		return resultatsMenages;
	
	
		
	}

	
	
}
