-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 09 juin 2021 à 00:04
-- Version du serveur :  10.4.17-MariaDB
-- Version de PHP : 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

DROP DATABASE IF EXISTS `covid_19_management`;
CREATE DATABASE `covid_19_management` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `covid_19_management`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `covid_19_management`
--

-- --------------------------------------------------------

--
-- Structure de la table `agent`
--

CREATE TABLE `agent` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(500) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `date_naissance` date NOT NULL,
  `sexe` varchar(80) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `profession` varchar(255) DEFAULT NULL,
  `est_contamine` tinyint(1) DEFAULT 0,
  `id_secteur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `agent`
--

INSERT INTO `agent` (`id`, `login`, `password`, `nom`, `prenom`, `date_naissance`, `sexe`, `image`, `profession`, `est_contamine`, `id_secteur`) VALUES
(1, 'maestros', 'maestros-password', 'Roslyn', 'Maestros-Roslyn', '2021-06-08', 'Maesculin', 'empty', 'Etudiant', 0, 1),
(17, 'Uri', 'maestros-password', 'Roslyn', 'Maestros-Roslyn', '2021-06-08', 'Maesculin', 'empty', 'Etudiant', 0, 2),
(18, 'Roslyn', 'maestros-password', 'Roslyn', 'Maestros-Roslyn', '2021-06-08', 'Maesculin', 'empty', 'Etudiant', 0, 3),
(19, 'Nelz', 'maestros-password', 'Roslyn', 'Maestros-Roslyn', '2021-06-08', 'Maesculin', 'empty', 'Etudiant', 0, 4),
(20, 'Marsvel', 'maestros-password', 'Roslyn', 'Maestros-Roslyn', '2021-06-08', 'Maesculin', 'empty', 'Etudiant', 0, 5),
(21, 'Ignami', 'maestros-password', 'Roslyn', 'Maestros-Roslyn', '2021-06-08', 'Maesculin', 'empty', 'Etudiant', 0, 6),
(22, 'Toto', 'maestros-password', 'Roslyn', 'Maestros-Roslyn', '2021-06-08', 'Maesculin', 'empty', 'Etudiant', 0, 7),
(23, 'le-toto', 'maestros-password', 'Roslyn', 'Maestros-Roslyn', '2021-06-08', 'Maesculin', 'empty', 'Etudiant', 0, 8);

-- --------------------------------------------------------

--
-- Structure de la table `citoyen`
--

CREATE TABLE `citoyen` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(500) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `date_naissance` date NOT NULL,
  `sexe` varchar(80) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `profession` varchar(255) DEFAULT NULL,
  `est_contamine` tinyint(1) DEFAULT NULL,
  `id_agent` int(11),
  `id_menage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `citoyen`
--

INSERT INTO `citoyen` (`id`, `login`, `password`, `nom`, `prenom`, `date_naissance`, `sexe`, `image`, `profession`, `est_contamine`, `id_agent`, `id_menage`) VALUES
(33, 'Maestros', 'maestros-password', 'Temateu Roslyn', 'Roslyn', '2021-06-08', 'Masculin', 'empty', 'Etudiant', 0, 1, 9),
(34, 'Lucie', 'maestros-password', 'Temateu Roslyn', 'Roslyn', '2021-06-08', 'Masculin', 'empty', 'Etudiant', 0, 17, 10),
(35, 'Shanks', 'maestros-password', 'Temateu Roslyn', 'Roslyn', '2021-06-08', 'Masculin', 'empty', 'Etudiant', 0, 18, 11),
(36, 'Lufy', 'maestros-password', 'Temateu Roslyn', 'Roslyn', '2021-06-08', 'Masculin', 'empty', 'Naruto', 0, 19, 12),
(37, 'Kaneki', 'maestros-password', 'Temateu Roslyn', 'Roslyn', '2021-06-08', 'Masculin', 'empty', 'Etudiant', 0, 20, 13),
(38, 'Solda-Hiver', 'maestros-password', 'Temateu Roslyn', 'Roslyn', '2021-06-08', 'Masculin', 'empty', 'Etudiant', 0, 21, 14),
(39, 'Toto', 'maestros-password', 'Temateu Roslyn', 'Roslyn', '2021-06-08', 'Masculin', 'empty', 'Etudiant', 0, 22, 15),
(40, 'Man-In-The-Middle', 'maestros-password', 'Temateu Roslyn', 'Roslyn', '2021-06-08', 'Masculin', 'empty', 'Etudiant', 0, 23, 16);

-- --------------------------------------------------------

--
-- Structure de la table `contamination`
--

CREATE TABLE `contamination` (
  `id_contamine` int(11) NOT NULL,
  `id_contagieux` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `denree`
--

CREATE TABLE `denree` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `denree`
--

INSERT INTO `denree` (`id`, `libelle`, `quantite`) VALUES
(1, 'Arachide', 100),
(2, 'viande sechee', 500),
(3, 'mais', 40),
(4, 'Tomate', 0),
(5, 'mais', 40),
(6, 'Tomate', 56),
(7, 'Coco', 122),
(8, 'Avocat Mbounda', 3231);

-- --------------------------------------------------------

--
-- Structure de la table `departement`
--

CREATE TABLE `departement` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `id_region` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `departement`
--

INSERT INTO `departement` (`id`, `nom`, `id_region`) VALUES
(1, 'Noun', 1),
(2, 'Miphi', 2),
(3, 'Centre', 3),
(4, 'NKongni', 4),
(5, 'Shiganshina', 5),
(6, 'Districk de Trost', 6),
(7, 'Tokyo', 7),
(8, 'Revenger', 8);

-- --------------------------------------------------------

--
-- Structure de la table `deptdenree`
--

CREATE TABLE `deptdenree` (
  `id_departement` int(11) NOT NULL,
  `id_denree` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `deptdenree`
--

INSERT INTO `deptdenree` (`id_departement`, `id_denree`, `quantite`) VALUES
(1, 1, 121),
(2, 2, 2324),
(3, 3, 233),
(4, 4, 242342),
(5, 5, 434),
(6, 6, 3434),
(7, 7, 4545),
(8, 8, 454545);

-- --------------------------------------------------------

--
-- Structure de la table `menage`
--

CREATE TABLE `menage` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `nombre_membre` int(11) NOT NULL,
  `localisation` varchar(255) DEFAULT NULL,
  `id_secteur` int(11) NOT NULL,
  `id_agent` int(11) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `menage`
--

INSERT INTO `menage` (`id`, `nom`, `nombre_membre`, `localisation`, `id_secteur`, `id_agent`) VALUES
(9, 'Complique', 23, 'Dschang', 1, 1),
(10, 'Complique', 23, 'Dschang', 2, 17),
(11, 'Simple', 3, 'Maroua', 3, 18),
(12, 'heurx', 5, 'Dschang', 4, 19),
(13, 'deplaisant', 23, 'Dschang', 5, 20),
(14, 'fake', 10, 'Yaoude', 6, 21),
(15, 'date', 6, 'Doula', 7, 22),
(16, 'tranquil', 4, 'Dschang', 8, 23);

-- --------------------------------------------------------

--
-- Structure de la table `menage_denree`
--

CREATE TABLE `menage_denree` (
  `id_menage` int(11) NOT NULL,
  `id_denree` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL,
  `id_agent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `menage_denree`
--

INSERT INTO `menage_denree` (`id_menage`, `id_denree`, `quantite`, `id_agent`) VALUES
(9, 1, 31313, 1),
(10, 2, 2424, 18),
(11, 3, 2535, 18),
(12, 4, 466, 19),
(13, 5, 74545, 20),
(14, 6, 7483, 21),
(15, 7, 3435, 22),
(16, 8, 3424, 23);

-- --------------------------------------------------------

--
-- Structure de la table `regdenree`
--

CREATE TABLE `regdenree` (
  `id_region` int(11) NOT NULL,
  `id_denree` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `regdenree`
--

INSERT INTO `regdenree` (`id_region`, `id_denree`, `quantite`) VALUES
(1, 1, 100),
(2, 2, 300),
(3, 3, 140495),
(4, 4, 4748),
(5, 5, 7438),
(6, 6, 7483),
(7, 7, 47384),
(8, 8, 43434);

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `region`
--

INSERT INTO `region` (`id`, `nom`) VALUES
(1, 'Bafoussam'),
(2, 'Dschang'),
(3, 'Nord'),
(4, 'Sud'),
(5, 'Est'),
(6, 'Ouest'),
(7, 'Nord-Ouest'),
(8, 'Sud-Ouest');

-- --------------------------------------------------------

--
-- Structure de la table `sectdenree`
--

CREATE TABLE `sectdenree` (
  `id_secteur` int(11) NOT NULL,
  `id_denree` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sectdenree`
--

INSERT INTO `sectdenree` (`id_secteur`, `id_denree`, `quantite`) VALUES
(1, 1, 43434),
(2, 2, 3434),
(3, 3, 445),
(4, 4, 2626),
(5, 5, 626442),
(6, 6, 626466),
(7, 7, 6246246),
(8, 8, 123434);

-- --------------------------------------------------------

--
-- Structure de la table `secteur`
--

CREATE TABLE `secteur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `id_deplacement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `secteur`
--

INSERT INTO `secteur` (`id`, `nom`, `id_deplacement`) VALUES
(1, 'Trost', 1),
(2, 'Avenu perdu', 2),
(3, 'Les retrouves', 3),
(4, 'les oublies', 4),
(5, 'les retrouves', 5),
(6, 'les absents', 6),
(7, 'les inoubliades', 7),
(8, 'les oublies', 8);

-- --------------------------------------------------------

--
-- Structure de la table `symptome`
--

CREATE TABLE `symptome` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `id_agent` int(11) NOT NULL,
  `id_citoyen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `symptome`
--

INSERT INTO `symptome` (`id`, `libelle`, `description`, `id_agent`, `id_citoyen`) VALUES
(1, 'Toux', 'fatigue et rhume', 1, 33),
(2, 'evanuissements', 'vertiges observes chez le patient', 17, 34),
(3, 'Toux', 'fatigue et rhume', 18, 35),
(4, 'Toux', 'fatigue et rhume', 19, 36),
(5, 'Toux', 'fatigue et rhume', 20, 37),
(6, 'Toux', 'fatigue et rhume', 21, 38),
(7, 'Toux', 'fatigue et rhume', 22, 39),
(8, 'Toux', 'fatigue et rhume', 23, 40);

-- --------------------------------------------------------

--
-- Structure de la table `voyage`
--

CREATE TABLE `voyage` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `lieu` varchar(255) DEFAULT NULL,
  `id_citoyen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `voyage`
--

INSERT INTO `voyage` (`id`, `date`, `lieu`, `id_citoyen`) VALUES
(2, '2021-06-09', 'Bafoussam', 33),
(3, '2021-06-09', 'Bafoussam', 34),
(4, '2021-06-09', 'Doula', 35),
(5, '2021-06-09', 'Konengui', 36),
(6, '2021-06-09', 'Keleng', 37),
(7, '2021-06-09', 'Bafi', 38),
(8, '2021-06-09', 'Yaoude', 39),
(9, '2021-06-09', 'Nkolo', 40);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `agent`
--
ALTER TABLE `agent`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD KEY `fk_agent_secteur` (`id_secteur`);

--
-- Index pour la table `citoyen`
--
ALTER TABLE `citoyen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD KEY `fk_citoyen_agent` (`id_agent`),
  ADD KEY `fk_citoyen_menage` (`id_menage`);

--
-- Index pour la table `contamination`
--
ALTER TABLE `contamination`
  ADD PRIMARY KEY (`id_contamine`,`id_contagieux`),
  ADD KEY `fk_contamination2_citoyen` (`id_contagieux`);

--
-- Index pour la table `denree`
--
ALTER TABLE `denree`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_departement_region` (`id_region`);

--
-- Index pour la table `deptdenree`
--
ALTER TABLE `deptdenree`
  ADD PRIMARY KEY (`id_departement`,`id_denree`),
  ADD KEY `fk_deptdenree_denree` (`id_denree`);

--
-- Index pour la table `menage`
--
ALTER TABLE `menage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_menage_secteur` (`id_secteur`),
  ADD KEY `fk_menage_agent` (`id_agent`);

--
-- Index pour la table `menage_denree`
--
ALTER TABLE `menage_denree`
  ADD PRIMARY KEY (`id_menage`,`id_denree`),
  ADD KEY `fk_menagedenree_denree` (`id_denree`),
  ADD KEY `fk_menagedenree_agent` (`id_agent`);

--
-- Index pour la table `regdenree`
--
ALTER TABLE `regdenree`
  ADD PRIMARY KEY (`id_region`,`id_denree`),
  ADD KEY `fk_regdenree_denree` (`id_denree`);

--
-- Index pour la table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sectdenree`
--
ALTER TABLE `sectdenree`
  ADD PRIMARY KEY (`id_secteur`,`id_denree`),
  ADD KEY `fk_sectdenree_denree` (`id_denree`);

--
-- Index pour la table `secteur`
--
ALTER TABLE `secteur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_secteur_departement` (`id_deplacement`);

--
-- Index pour la table `symptome`
--
ALTER TABLE `symptome`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_symptome_agent` (`id_agent`),
  ADD KEY `fk_symptome_citoyen` (`id_citoyen`);

--
-- Index pour la table `voyage`
--
ALTER TABLE `voyage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_symptome_citoyens` (`id_citoyen`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `agent`
--
ALTER TABLE `agent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `citoyen`
--
ALTER TABLE `citoyen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT pour la table `denree`
--
ALTER TABLE `denree`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `departement`
--
ALTER TABLE `departement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `menage`
--
ALTER TABLE `menage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `secteur`
--
ALTER TABLE `secteur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `symptome`
--
ALTER TABLE `symptome`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `voyage`
--
ALTER TABLE `voyage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `agent`
--
ALTER TABLE `agent`
  ADD CONSTRAINT `fk_agent_secteur` FOREIGN KEY (`id_secteur`) REFERENCES `secteur` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `citoyen`
--
ALTER TABLE `citoyen`
  ADD CONSTRAINT `fk_citoyen_agent` FOREIGN KEY (`id_agent`) REFERENCES `agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_citoyen_menage` FOREIGN KEY (`id_menage`) REFERENCES `menage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `contamination`
--
ALTER TABLE `contamination`
  ADD CONSTRAINT `fk_contamination1_citoyen` FOREIGN KEY (`id_contamine`) REFERENCES `citoyen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contamination2_citoyen` FOREIGN KEY (`id_contagieux`) REFERENCES `citoyen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `departement`
--
ALTER TABLE `departement`
  ADD CONSTRAINT `fk_departement_region` FOREIGN KEY (`id_region`) REFERENCES `region` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `deptdenree`
--
ALTER TABLE `deptdenree`
  ADD CONSTRAINT `fk_deptdenree_denree` FOREIGN KEY (`id_denree`) REFERENCES `denree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_deptdenree_departement` FOREIGN KEY (`id_departement`) REFERENCES `departement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `menage`
--
ALTER TABLE `menage`
  ADD CONSTRAINT `fk_menage_agent` FOREIGN KEY (`id_agent`) REFERENCES `agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_menage_secteur` FOREIGN KEY (`id_secteur`) REFERENCES `secteur` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `menage_denree`
--
ALTER TABLE `menage_denree`
  ADD CONSTRAINT `fk_menagedenree_agent` FOREIGN KEY (`id_agent`) REFERENCES `agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_menagedenree_denree` FOREIGN KEY (`id_denree`) REFERENCES `denree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_menagedenree_menage` FOREIGN KEY (`id_menage`) REFERENCES `menage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `regdenree`
--
ALTER TABLE `regdenree`
  ADD CONSTRAINT `fk_regdenree_denree` FOREIGN KEY (`id_denree`) REFERENCES `denree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_regdenree_region` FOREIGN KEY (`id_region`) REFERENCES `region` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sectdenree`
--
ALTER TABLE `sectdenree`
  ADD CONSTRAINT `fk_sectdenree_denree` FOREIGN KEY (`id_denree`) REFERENCES `denree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sectdenree_secteur` FOREIGN KEY (`id_secteur`) REFERENCES `secteur` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `secteur`
--
ALTER TABLE `secteur`
  ADD CONSTRAINT `fk_secteur_departement` FOREIGN KEY (`id_deplacement`) REFERENCES `departement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `symptome`
--
ALTER TABLE `symptome`
  ADD CONSTRAINT `fk_symptome_agent` FOREIGN KEY (`id_agent`) REFERENCES `agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_symptome_citoyen` FOREIGN KEY (`id_citoyen`) REFERENCES `citoyen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `voyage`
--
ALTER TABLE `voyage`
  ADD CONSTRAINT `fk_symptome_citoyens` FOREIGN KEY (`id_citoyen`) REFERENCES `citoyen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
