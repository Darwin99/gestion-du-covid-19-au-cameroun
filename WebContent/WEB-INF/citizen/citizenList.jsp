
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.covid.beans.Citoyen"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html style="height: auto;">
<head>
<meta charset="ISO-8859-1">
<title>Ajout d'un citoyen | 237 Covid on gere</title>

<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet"
	href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<!-- iCheck -->
<link rel="stylesheet"
	href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- JQVMap -->
<link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="dist/css/adminlte.min.css">
<!-- overlayScrollbars -->
<link rel="stylesheet"
	href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
<!-- Daterange picker -->
<link rel="stylesheet"
	href="plugins/daterangepicker/daterangepicker.css">
<!-- summernote -->
<link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">



<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../../plugins/fontawesome-free/css/all.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet"
	href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet"
	href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">



</head>
<body class="sidebar-mini layout-fixed" style="height: auto;">


	<div class="wrapper">



		<%@ include file="/WEB-INF/navbar.jsp"%>

		<%@ include file="/WEB-INF/sidebar.jsp"%>








		<div class="content-wrapper" style="min-height: 179.767px;">
			<!-- Content Header (Page header) -->
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1 class="m-0">Liste de Citoyens</h1>
						</div>
						<!-- /.col -->
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="#">Dashbord</a></li>
								<li class="breadcrumb-item active">Liste de Citoyens</li>
							</ol>

						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12">





							<div class="card">
								<div class="card-header">
									<h3 class="card-title">Liste des citoyens</h3>
								</div>
								<!-- /.card-header -->
								<div class="card-body">
									<div id="example1_wrapper"
										class="dataTables_wrapper dt-bootstrap4">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<div class="dt-buttons btn-group flex-wrap">
													<button
														class="btn btn-secondary buttons-copy buttons-html5"
														tabindex="0" aria-controls="example1" type="button">
														<span>Copy</span>
													</button>
													<button class="btn btn-secondary buttons-csv buttons-html5"
														tabindex="0" aria-controls="example1" type="button">
														<span>CSV</span>
													</button>
													<button
														class="btn btn-secondary buttons-excel buttons-html5"
														tabindex="0" aria-controls="example1" type="button">
														<span>Excel</span>
													</button>
													<button class="btn btn-secondary buttons-pdf buttons-html5"
														tabindex="0" aria-controls="example1" type="button">
														<span>PDF</span>
													</button>
													<button class="btn btn-secondary buttons-print"
														tabindex="0" aria-controls="example1" type="button">
														<span>Print</span>
													</button>
													<div class="btn-group">
														<button
															class="btn btn-secondary buttons-collection dropdown-toggle buttons-colvis"
															tabindex="0" aria-controls="example1" type="button"
															aria-haspopup="true" aria-expanded="true">
															<span>Column visibility</span>
														</button>
														<div class="dt-button-background" style=""></div>
														<div class="dt-button-collection"
															style="top: 38.2333px; left: 0px;">
															<div class="dropdown-menu" role="menu">
																<a
																	class="dt-button dropdown-item buttons-columnVisibility active"
																	tabindex="0" aria-controls="example1" href="#"
																	data-cv-idx="0"><span>Rendering engine</span></a> <a
																	class="dt-button dropdown-item buttons-columnVisibility active"
																	tabindex="0" aria-controls="example1" href="#"
																	data-cv-idx="1"><span>Browser</span></a> <a
																	class="dt-button dropdown-item buttons-columnVisibility active"
																	tabindex="0" aria-controls="example1" href="#"
																	data-cv-idx="2"><span>Platform(s)</span></a> <a
																	class="dt-button dropdown-item buttons-columnVisibility active"
																	tabindex="0" aria-controls="example1" href="#"
																	data-cv-idx="3"><span>Engine version</span></a> <a
																	class="dt-button dropdown-item buttons-columnVisibility active"
																	tabindex="0" aria-controls="example1" href="#"
																	data-cv-idx="4"><span>CSS grade</span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-12 col-md-6">
												<div id="example1_filter" class="dataTables_filter">
													<label>Search:<input type="search"
														class="form-control form-control-sm" placeholder=""
														aria-controls="example1"></label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<table id="example1"
													class="table table-bordered table-striped dataTable dtr-inline"
													role="grid" aria-describedby="example1_info">
													<thead>
														<tr role="row">
															<th class="sorting sorting_asc" tabindex="0"
																aria-controls="example1" rowspan="1" colspan="1"
																aria-label="Nom: activate to sort column descending"
																aria-sort="ascending">Nom</th>
															<th class="sorting" tabindex="0" aria-controls="example1"
																rowspan="1" colspan="1"
																aria-label="Date de naissance activate to sort column ascending"
																style="">Date de naissance</th>
															<th class="sorting" tabindex="0" aria-controls="example1"
																rowspan="1" colspan="1"
																aria-label="Profession(s): activate to sort column ascending"
																style="">Profession(s)</th>
															<th class="sorting" tabindex="0" aria-controls="example1"
																rowspan="1" colspan="1"
																aria-label="Telephone: activate to sort column ascending"
																style="">Telephone</th>
																<th class="sorting" tabindex="0" aria-controls="example1"
																rowspan="1" colspan="1"
																aria-label="Actions: activate to sort column ascending"
																style="">Symptomes</th>
															<th class="sorting" tabindex="0" aria-controls="example1"
																rowspan="1" colspan="1"
																aria-label="Actions: activate to sort column ascending"
																style="">Actions</th>
														</tr>
													</thead>
													<tbody>
														<%
															ArrayList<Citoyen> citoyens = (ArrayList<Citoyen>) request.getAttribute("citoyens");
														%>

														<%
															for (com.covid.beans.Citoyen citoyen : citoyens) {
														%>

														<tr class="odd">
															<td class="dtr-control sorting_1" tabindex="0">
																<%
																	out.print(citoyen.getNom() + " " + citoyen.getPrenom());
																%>
															</td>




															<td style="">
																<%
																	out.print(citoyen.getDateNaissance());
																%>
															</td>
															<td style="">
																<%
																	out.print(citoyen.getProfession());
																%>
															</td>
															<td style="">243 366 594</td>
															<td style=""><button type="button" class="btn btn-primary"
																	data-toggle="modal" data-target="#sy<%out.print(citoyen.getId());%>">
																	ajouter</button>

																<div id="sy<%out.print(citoyen.getId());%>" class="modal" tabindex="-1"
																	role="dialog">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<div class="modal-header">
																				<h5 class="modal-title">Ajout des symptomes du
																					citoyen</h5>
																				<button type="button" class="close"
																					data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">&times;</span>
																				</button>
																			</div>
																			<div class="modal-body">
																				<form action= "addSymptomes" method="POST">

																					<div class="form-check form-switch">
																						<input class="form-check-input" type="checkbox" name="fievre"
																							id="flexSwitchCheckDefault" value="fievre"> <label
																							class="form-check-label"
																							for="flexSwitchCheckDefault">Fievre</label>
																					</div>
																					<div class="form-check form-switch">
																						<input class="form-check-input" type="checkbox" name="toux_seche"
																							id="flexSwitchCheckDefault"> <label
																							class="form-check-label"
																							for="flexSwitchCheckDefault">Toux seche</label>
																					</div>
																					<div class="form-check form-switch">
																						<input class="form-check-input" type="checkbox" name="fatigue"
																							id="flexSwitchCheckDefault"> <label
																							class="form-check-label"
																							for="flexSwitchCheckDefault">Fatigue</label>
																					</div>
																					<div class="form-check form-switch">
																						<input class="form-check-input" type="checkbox" name="courbature"
																							id="flexSwitchCheckDefault"> <label
																							class="form-check-label"
																							for="flexSwitchCheckDefault">courbature</label>
																					</div>
																					<div class="form-check form-switch">
																						<input class="form-check-input" type="checkbox" name="perte_de_gout"
																							id="flexSwitchCheckDefault"> <label
																							class="form-check-label"
																							for="flexSwitchCheckDefault">Perte de
																							gout</label>
																					</div>
																					<div class="form-check form-switch">
																						<input class="form-check-input" type="checkbox" name="difficulte_respiratoire"
																							id="flexSwitchCheckDefault"> <label
																							class="form-check-label"
																							for="flexSwitchCheckDefault">Difficulte
																							respiratoire</label>
																					</div>
																					<div class="form-check form-switch">
																						<input class="form-check-input" type="checkbox" name="maux_de_gorge"
																							id="flexSwitchCheckDefault"> <label
																							class="form-check-label"
																							for="flexSwitchCheckDefault">maux de
																							gorge</label>
																					</div>
																					<div class="form-check form-switch">
																						<input class="form-check-input" type="checkbox" name="diarrhee"
																							id="flexSwitchCheckDefault"> <label
																							class="form-check-label"
																							for="flexSwitchCheckDefault">diarrhee</label>
																					</div>
																					<div class="form-check form-switch">
																						<input class="form-check-input" type="hidden" name="id_citoyen"
																							id="flexSwitchCheckDefault" value="<%out.print(citoyen.getId());%>">
																							
																					</div>
																			</div>
																			<div class="modal-footer">
																				<button type="submit" class="btn btn-primary">Enregistrer</button>
																				</form>
																				<button type="button" class="btn btn-secondary"
																					data-dismiss="modal">Close</button>
																			</div>
																		</div>
																	</div>
																</div></td>
															<td style=""><a class="fa fa-edit"
																href="updateCitizen.html?id= <%out.print(citoyen.getId());%>"></a>
																<a class="fa fa-trash text-danger"
																href="deleteCitizen.html?id=<%out.print(citoyen.getId());%>"></a>
																</td>
														</tr>

														<%
															}
														%>

													</tbody>
													<tfoot>
														<tr>
															<th rowspan="1" colspan="1">Nom</th>
															<th rowspan="1" colspan="1" style="">Date de
																naissance</th>
															<th rowspan="1" colspan="1" style="">profession(s)</th>
															<th rowspan="1" colspan="1" style="">Telephone</th>
															<th rowspan="1" colspan="1" style="">Actions</th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 col-md-5">
												<div class="dataTables_info" id="example1_info"
													role="status" aria-live="polite">Showing 1 to 10 of
													57 entries</div>
											</div>
											<div class="col-sm-12 col-md-7">
												<div class="dataTables_paginate paging_simple_numbers"
													id="example1_paginate">
													<ul class="pagination">
														<li class="paginate_button page-item previous disabled"
															id="example1_previous"><a href="#"
															aria-controls="example1" data-dt-idx="0" tabindex="0"
															class="page-link">Previous</a></li>
														<li class="paginate_button page-item active"><a
															href="#" aria-controls="example1" data-dt-idx="1"
															tabindex="0" class="page-link">1</a></li>
														<li class="paginate_button page-item "><a href="#"
															aria-controls="example1" data-dt-idx="2" tabindex="0"
															class="page-link">2</a></li>
														<li class="paginate_button page-item "><a href="#"
															aria-controls="example1" data-dt-idx="3" tabindex="0"
															class="page-link">3</a></li>
														<li class="paginate_button page-item "><a href="#"
															aria-controls="example1" data-dt-idx="4" tabindex="0"
															class="page-link">4</a></li>
														<li class="paginate_button page-item "><a href="#"
															aria-controls="example1" data-dt-idx="5" tabindex="0"
															class="page-link">5</a></li>
														<li class="paginate_button page-item "><a href="#"
															aria-controls="example1" data-dt-idx="6" tabindex="0"
															class="page-link">6</a></li>
														<li class="paginate_button page-item next"
															id="example1_next"><a href="#"
															aria-controls="example1" data-dt-idx="7" tabindex="0"
															class="page-link">Next</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /.card-body -->
							</div>





						</div>

					</div>


				</div>
				<!-- /.row (main row) -->
				<!-- /.container-fluid -->
			</section>
			<!-- /.content -->
		</div>



















	</div>

	<%@ include file="/WEB-INF/footer.jsp"%>


	<!-- jQuery -->
	<script src="plugins/jquery/jquery.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button)
	</script>
	<!-- Bootstrap 4 -->
	<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- ChartJS -->
	<script src="plugins/chart.js/Chart.min.js"></script>
	<!-- Sparkline -->
	<script src="plugins/sparklines/sparkline.js"></script>
	<!-- JQVMap -->
	<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
	<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
	<!-- jQuery Knob Chart -->
	<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
	<!-- daterangepicker -->
	<script src="plugins/moment/moment.min.js"></script>
	<script src="plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script
		src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
	<!-- Summernote -->
	<script src="plugins/summernote/summernote-bs4.min.js"></script>
	<!-- overlayScrollbars -->
	<script
		src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/adminlte.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="dist/js/demo.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="dist/js/pages/dashboard.js"></script>


	<!-- jQuery -->
	<script src="../../plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- DataTables  & Plugins -->
	<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
	<script
		src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script
		src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
	<script
		src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
	<script
		src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
	<script
		src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
	<script src="../../plugins/jszip/jszip.min.js"></script>
	<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
	<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
	<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
	<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
	<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- Page specific script -->
	<script>
		$(function() {
			$("#example1").DataTable(
					{
						"responsive" : true,
						"lengthChange" : false,
						"autoWidth" : false,
						"buttons" : [ "copy", "csv", "excel", "pdf", "print",
								"colvis" ]
					}).buttons().container().appendTo(
					'#example1_wrapper .col-md-6:eq(0)');
			$('#example2').DataTable({
				"paging" : true,
				"lengthChange" : false,
				"searching" : false,
				"ordering" : true,
				"info" : true,
				"autoWidth" : false,
				"responsive" : true,
			});
		});
	</script>

</body>
</html>