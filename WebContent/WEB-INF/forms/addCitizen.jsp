
<%@page import="com.covid.beans.Citoyen"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html style="height: auto;">
<head>
<meta charset="ISO-8859-1">
<title>Ajout d'un citoyen | 237 Covid on gere</title>

<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet"
	href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<!-- iCheck -->
<link rel="stylesheet"
	href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- JQVMap -->
<link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="dist/css/adminlte.min.css">
<!-- overlayScrollbars -->
<link rel="stylesheet"
	href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
<!-- Daterange picker -->
<link rel="stylesheet"
	href="plugins/daterangepicker/daterangepicker.css">
<!-- summernote -->
<link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
</head>
<body class="sidebar-mini layout-fixed" style="height: auto;">


	<div class="wrapper">


		<%@ include file="/WEB-INF/navbar.jsp"%>

		<%@ include file="/WEB-INF/sidebar.jsp"%>








		<div class="content-wrapper" style="min-height: 179.767px;">
			<!-- Content Header (Page header) -->
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1 class="m-0">Ajout de citoyen</h1>
						</div>
						<!-- /.col -->
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="#">Dashbord</a></li>
								<li class="breadcrumb-item active">Ajout d'un citoyen</li>
							</ol>
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div>
							<span style="color:green;"> ${success}</span> <span style="color: red;"> ${failled} </span>
						</div>

						<!-- // Basic multiple Column Form section start -->
						<section id="multiple-column-form">
							<div class="row match-height">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h4 class="card-title"></h4>
										</div>
										<div class="card-content">
											<div class="card-body">
												<form class="form" method="post" action="addCitizen.html">
													<div class="row">
														<div class="col-md-6 col-12">
															<div class="form-group">
																<label for="prenom">Prenom </label> <input type="text"
																	id="prenom" class="form-control" placeholder="Prenom"
																	name="prenom" value="${citoyen.getPrenom() }"> <br> <span
																	style="color: red;" class="error">${errors['prenom']}</span>
															</div>
														</div>
														<div class="col-md-6 col-12">
															<div class="nom">
																<label for="last-name-column">Nom <span
																	style="color: red;">*</span></label> <input type="text"
																	id="nom" class="form-control" placeholder="Nom"
																	name="nom" required="required"
																	value='<c:out value="${citoyen.getNom()}"></c:out>'>
																<br> <span style="color: red;" class="error">${errors['nom']}</span>
															</div>
														</div>
														<div class="col-md-6 col-12">
															<div class="form-group">
																<label for="dateDeNaissance">Date de Naissance <span
																	style="color: red;">*</span></label> <input type="date"
																	id="dateDeNaissance" class="form-control"
																	placeholder="Date de Naissance" name="dateDeNaissance"
																	required="required" value='<c:out value="${citoyen.getDateNaissance()}"></c:out>'>
															</div>
														</div>
														<div class="col-md-6 col-12">
															<div class="form-group">
																<label for="profession">Profession <span
																	style="color: red;">*</span></label> <input
																	type="text" id="proffesion" class="form-control"
																	name="proffession" placeholder="Profession..." value='<c:out value="${citoyen.getProfession()}"></c:out>'>
																	<br><span
																	style="color: red;">*${errors['profession']}</span>
															</div>

														</div>

														<div class="col-md-6 col-12">
															<div class="form-group">
																<label for="image">Image <span
																	style="color: red;">*</span></label> <input type="file"
																	id="image" class="form-control" name="image" value='<c:out value="${citoyen.getImage()}"></c:out>'>
															</div>

														</div>

														<%-- <div class="col-md-6 col-12">
															<div class="form-group">
																<label for="email">Email <span
																	style="color: red;">*</span></label> <input
																	type="email" id="email" class="form-control"
																	name="email" placeholder="Email..." value='<c:out value="${citoyen.getEmail()}"></c:out>'>> <br>
																<span style="color: red;">*${errors['email']}</span>
															</div>

														</div> --%>
														<div class="col-md-6 col-12">
															<div class="form-group">
																<label for="menage">Menage <span
																	style="color: red;">*</span></label> <input type="text"
																	id="menage" class="form-control" name="menage" value='<c:out value="${citoyen.getMenage().getId()}"></c:out>'>>


															</div>

														</div>


														<div class="col-md-6 col-12">
															<div class="form-group">
																<label for="login">Login <span
																	style="color: red;">*</span></label> <input type="text"
																	id="login" class="form-control" name="login"
																	placeholder="Login..." required="required" value='<c:out value="${citoyen.getNom()}"></c:out>'>> <br>
																<span style="color: red;">${errors['login']}</span>
															</div>

														</div>

														<div class="col-md-6 col-12">
															<div class="form-group">
																<label for="password">Password <span
																	style="color: red;">*</span></label> <input type="password"
																	id="proffesion" class="form-control" name="password"
																	placeholder="password" required="required"> <br>
																<span style="color: red;">${errors['Password']}</span>
															</div>

														</div>
														<div class="col-md-6 col-12">
															<div class="form-group">
																<label for="confirmation">Confirmation <span
																	style="color: red;">*</span></label> <input type="password"
																	id="proffesion" class="form-control"
																	name="confirmation" placeholder="" required="required">
																<br> <span style="color: red;">${errors['password']}</span>
															</div>

														</div>
														<!-- <div class="col-md-6 col-12">
													<div class="form-group">
														<label for="email-id-column">Email</label> <input
															type="email" id="email-id-column" class="form-control"
															name="email-id-column" placeholder="Email">
													</div>
												</div> -->

														<div class="col-md-6 col-12">
															<div class="form-group form-inline">
																<label for="sexe">Sexe: <span
																	style="color: red;">*   </span> <span style="width: 10px; "> </span>
																</label> <input
																	class=" form-check "
																	type="radio" value="M" name="sexe" id="sexe" checked>M
																<span style="width: 10px; "> </span> <input class=""
																	type="radio" name="sexe" value="F" id="sexe"> F
															</div>
														</div>
														
														<!-- <div class="form-group col-12">
															<div class='form-check'>
																<div class="checkbox">
																	<input type="checkbox" id="checkbox5"
																		class='form-check-input' checked> <label
																		for="checkbox5">Remember Me</label>
																</div>
															</div>
														</div> -->
														
		
														
														<div class="col-12 d-flex justify-content-end">
															<button type="submit" class="btn btn-primary me-1 mb-1">Submit</button>
															<button type="reset"
																class="btn btn-light-secondary me-1 mb-1">Reset</button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<!-- // Basic multiple Column Form section end -->

					</div>


				</div>
				<!-- /.row (main row) -->
				<!-- /.container-fluid -->
			</section>
			<!-- /.content -->
		</div>



















	</div>

	<%@ include file="/WEB-INF/footer.jsp"%>


	<!-- jQuery -->
	<script src="plugins/jquery/jquery.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button)
	</script>
	<!-- Bootstrap 4 -->
	<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- ChartJS -->
	<script src="plugins/chart.js/Chart.min.js"></script>
	<!-- Sparkline -->
	<script src="plugins/sparklines/sparkline.js"></script>
	<!-- JQVMap -->
	<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
	<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
	<!-- jQuery Knob Chart -->
	<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
	<!-- daterangepicker -->
	<script src="plugins/moment/moment.min.js"></script>
	<script src="plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script
		src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
	<!-- Summernote -->
	<script src="plugins/summernote/summernote-bs4.min.js"></script>
	<!-- overlayScrollbars -->
	<script
		src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/adminlte.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="dist/js/demo.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="dist/js/pages/dashboard.js"></script>

</body>
</html>