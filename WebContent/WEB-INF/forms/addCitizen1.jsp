
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>



<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ajout d'un Citoyen| 237 Covid on gere</title>

<link rel="preconnect" href="https://fonts.gstatic.com">
<link
	href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap"
	rel="stylesheet">
<link rel="stylesheet" href="assets/css/bootstrap.css">

<link rel="stylesheet"
	href="assets/vendors/perfect-scrollbar/perfect-scrollbar.css">
<link rel="stylesheet"
	href="assets/vendors/bootstrap-icons/bootstrap-icons.css">
<link rel="stylesheet" href="assets/css/app.css">
<link rel="shortcut icon" href="assets/images/favicon.svg"
	type="image/x-icon">
</head>

<body>
	<div id="app">

		<%@ include file="/WEB-INF/sidebar.jsp"%>

		<div id="main">
			<header class="mb-3">
				<a href="#" class="burger-btn d-block d-xl-none"> <i
					class="bi bi-justify fs-3"></i>
				</a>
			</header>

			<div class="page-heading">
				<div class="page-title">
					<div class="row">
						<div class="col-12 col-md-6 order-md-1 order-last">
							<h3>Add Citizen</h3>
							<p class="text-subtitle text-muted">Citizen registring</p>
						</div>
						<div class="col-12 col-md-6 order-md-2 order-first">
							<nav aria-label="breadcrumb"
								class="breadcrumb-header float-start float-lg-end">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
									<li class="breadcrumb-item active" aria-current="page">Add
										Citizen</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>



				<!-- // Basic multiple Column Form section start -->
				<section id="multiple-column-form">
					<div class="row match-height">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title"></h4>
								</div>
								<div class="card-content">
									<div class="card-body">
										<form class="form" method="post" action="addCitizen.html">
											<div class="row">
												<div class="col-md-6 col-12">
													<div class="form-group">
														<label for="prenom">Prenom </label> <input
															type="text" id="prenom" class="form-control"
															placeholder="Prenom" name="prenom"> <br> <span
															class="error">${erreurs['prenom']}</span>
													</div>
												</div>
												<div class="col-md-6 col-12">
													<div class="nom">
														<label for="last-name-column">Nom <span
															style="color: red;">*</span></label> <input type="text" id="nom"
															class="form-control" placeholder="Nom" name="nom"
															required="required" value='<c:out value="{{param.citoyen.getNom()}}"></c:out>'>
													</div>
												</div>
												<div class="col-md-6 col-12">
													<div class="form-group">
														<label for="dateDeNaissance">Date de Naissance <span
															style="color: red;">*</span></label> <input type="date"
															id="dateDeNaissance" class="form-control"
															placeholder="Date de Naissance" name="dateDeNaissance"
															required="required">
													</div>
												</div>
												<div class="col-md-6 col-12">
													<div class="form-group">
														<label for="profession">Profession</label> <input
															type="text" id="proffesion" class="form-control"
															name="proffession" placeholder="Profession">
													</div>

												</div>

												<div class="col-md-6 col-12">
													<div class="form-group">
														<label for="image">Image <span
															style="color: red;">*</span></label> <input type="file"
															id="image" class="form-control" name="image"
															>
													</div>

												</div>

												<div class="col-md-6 col-12">
													<div class="form-group">
														<label for="login">Login <span
															style="color: red;">*</span></label> <input type="text"
															id="login" class="form-control" name="login"
															placeholder="Login..." required="required">
													</div>

												</div>

												<div class="col-md-6 col-12">
													<div class="form-group">
														<label for="password">Password <span
															style="color: red;">*</span></label> <input type="password"
															id="proffesion" class="form-control" name="password"
															placeholder="password" required="required">
													</div>

												</div>
												<div class="col-md-6 col-12">
													<div class="form-group">
														<label for="confirmation">Confirmation <span
															style="color: red;">*</span></label> <input type="password"
															id="proffesion" class="form-control" name="confirmation"
															placeholder="" required="required">
													</div>

												</div>
												<!-- <div class="col-md-6 col-12">
													<div class="form-group">
														<label for="email-id-column">Email</label> <input
															type="email" id="email-id-column" class="form-control"
															name="email-id-column" placeholder="Email">
													</div>
												</div> -->

												<div class="col-md-6 col-12">
													<div class="form-group">
														<label for="sexe">Sexe: <span
															style="color: red;">*</span>
														</label><input
															class="form-check-input form-check form-check-primary"
															type="radio" name="sexe" id="sexe" checked>Masculin
														<input class="form-check-input" type="radio" name="sexe"
															id="sexe"> Femin
													</div>
												</div>
												<a>Ajouter une famile</a>
												<a>Creer une famille</a>
												<div class="form-group col-12">
													<div class='form-check'>
														<div class="checkbox">
															<input type="checkbox" id="checkbox5"
																class='form-check-input' checked> <label
																for="checkbox5">Remember Me</label>
														</div>
													</div>
												</div>
												<div class="col-12 d-flex justify-content-end">
													<button type="submit" class="btn btn-primary me-1 mb-1">Submit</button>
													<button type="reset"
														class="btn btn-light-secondary me-1 mb-1">Reset</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- // Basic multiple Column Form section end -->
			</div>

			<footer>
				<div class="footer clearfix mb-0 text-muted">
					<div class="float-start">
						<p>2021 &copy; Mazer</p>
					</div>
					<div class="float-end">
						<p>
							Crafted with <span class="text-danger"><i
								class="bi bi-heart"></i></span> by <a href="http://ahmadsaugi.com">Cameroon
								UDS INF3 Students</a>
						</p>
					</div>
				</div>
			</footer>
		</div>
	</div>
	<script src="assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	<script src="assets/js/bootstrap.bundle.min.js"></script>

	<script src="assets/js/main.js"></script>
</body>



</html>