<footer class="main-footer">
	<div class="">
		<p>
			Crafted with <span class="text-danger"><i class="bi bi-heart"></i></span>
			by <a href="http://ahmadsaugi.com">Cameroon UDS INF3 Students 2021</a>
		</p>
	</div>

	<strong>Copyright � 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.
	</strong> All rights reserved.
	<div class="float-right d-none d-sm-inline-block">
		<b>Version</b> 3.1.0
	</div>
</footer>
